# Recap Time Telegram Bot Source Docs


## What We use
The documentation site uses [`gatsby-starter-markdown`](https://cvluca.github.io/gatsby-starter-markdown) template to

## Quick Start

### For contributors: Cloning the whole repo [The Most Recommended Way]
1.  **Download the source code.**
    ```sh
    git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/RecapTime-Staff/tgbot-source-docs
    ```

2. **Open the directory where you cloned the repo.**
    ```sh
    cd ./tgbot-source-docs
    gastby develop
    ```

3.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    *Note: You'll also see a second link: `http://localhost:8000/___graphql`. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql).*

4.  **Check for status, then pull or rebase**
    ```sh
    # Check for status first
    git status

    # Fetch updates from origin/master
    git fetch origin/master master

    # For forks, either rebase or pull then resolve conflicts.
    git pull
    git rebase
    ```

### For building mirrors: Using npm [Not Recommended]
1.  **Install the package**
    ```sh
    # Pulls last released package from registry.npmjs.com
    npm install @MadeByThePinsTeam/tgbot-source-docs
    ```

2.  **Just hit the road!**
    ```sh
    # Just hit start to load. This will run the dev server.
    npm start

    # Alternatively, start building the resources.
    npm run build
    ```
